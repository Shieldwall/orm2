import psycopg2
import psycopg2.extras


class DatabaseError(Exception):
    pass


class NotFoundError(Exception):
    pass


class ModifiedError(Exception):
    pass


class Entity(object):
    db = None

    __delete_query = 'DELETE FROM "{table}" WHERE {table}_id=%s'
    __insert_query = 'INSERT INTO "{table}" ({columns}) VALUES ({placeholders}) RETURNING "{table}_id"'
    __list_query = 'SELECT * FROM "{table}"'
    __select_query = 'SELECT * FROM "{table}" WHERE {table}_id=%s'
    __update_query = 'UPDATE "{table}" SET {columns} WHERE {table}_id=%s'

    __parent_query = 'SELECT * FROM "{table}" WHERE {parent}_id=%s'
    __sibling_query = 'SELECT * FROM "{sibling}" NATURAL JOIN "{join_table}" WHERE {table}_id=%s'

    def __init__(self, id=None):
        if self.__class__.db is None:
            raise DatabaseError()

        self.__cursor = self.__class__.db.cursor(
            cursor_factory=psycopg2.extras.RealDictCursor
        )
        self.__fields = {}
        self.__id = id
        self.__loaded = False
        self.__modified = False
        self.__table = self.__class__.__name__.lower()

    def __getattr__(self, name):
        if self.__modified:
            raise ModifiedError()

        self.__load()

        if name in self.__class__._columns:
            return self._get_column(name)
        elif name in self.__class__._parents:
            return _get_parent(name)
        elif name in self.__class__._children:
            return _get_children(name)
        elif name in self.__class__._siblings:
            return _get_siblings(name)
        else:
            raise AttributeError()

    def __setattr__(self, name, value):
        if name in self.__class__._columns:
            self._set_column(name, value)
        elif name in self.__class__._parents:
            self._set_parent(name, value)
        else:
            super(Entity, self).__setattr__(name, value)

    def __execute_query(self, query, args=None):
        result = None

        try:
            self.__cursor.execute(query, args)
            result = self.__cursor.fetchall()
            self.__class__.db.commit()
        except psycopg2.ProgrammingError:
            pass  # Catches error from fetchall in case of UPDATE or DELETE
        except:
            self.__class__.db.rollback()
            raise

        return result

    def __insert(self):
        placeholders = []
        values = []
        fields = []

        for field, value in self.__fields.iteritems():
            placeholders.append('%s')
            values.append(value)
            fields.append(field)

        query = self.__class__.__insert_query.format(
            table=self.__table,
            columns=', '.join(fields),
            placeholders=', '.join(placeholders)
        )
        result = self.__execute_query(query, values)[0]
        self.id = result[self._convert_id(self.__table)]

    def __load(self):
        if self.__loaded or self.id is None:
            return

        query = self.__class__.__select_query.format(
            table=self.__table)
        row = self.__execute_query(query, [self.id])[0]

        for key, value in row.iteritems():
            self.__fields[key] = value

        self.__loaded = True
        self.__modified = False

    def __update(self):
        placeholders = []
        values = []
        for field, value in self.__fields.iteritems():
            placeholders.append('"{field}" = %s'.format(field=field))
            values.append(value)

        values.append(self.id)
        query = self.__class__.__update_query.format(
            table=self.__table,
            columns=', '.join(placeholders)
        )
        self.__execute_query(query, values)

    def _get_column(self, name):
        return self.__fields[self._convert_name(name)]

    def _get_children(self, name):
        classname = self._children[name]

        query = self.__parent_query.format(
            table=classname.lower(),
            parent=self.__table
        )
        row_list = self.__execute_query(query, [self.id])

        return self._instance_multiload(self._get_class(classname), row_list)

    def _get_parent(self, name):
        parent_id = self.__fields[self._convert_id(name)]

        instance = self._get_class(name.capitalize())(parent_id)
        query = self.__parent_query.format(
            table=name,
            parent=name
        )
        row = self.__execute_query(query, [parent_id])

        self._instance_load(instance, row)

        return instance

    def _get_siblings(self, name):
        classname = self._siblings[name]
        tables = sorted(self.__table, classname.lower())

        query = self.__sibling_query.format(
            sibling=classname.lower(),
            join_table='{left}__{right}'.format(
                left=tables[0],
                right=tables[1]
            ),
            table=self.__table
        )
        row_list = self.__execute_query(query, [self.id])

        return self._instance_multiload(self._get_class(classname), row_list)

    def _set_parent(self, name, value):
        from numbers import Number

        if isinstance(value, Number) or isnumeric(value):
            self.__fields[self._convert_id(name)] = value
        elif issubclass(value, Entity):
            self.__fields[self._convert_id(name)] = value.id
        else:
            raise AttributeError()

    def _set_column(self, name, value):
        self.__fields[self._convert_name(name)] = value
        self.__modified = True

    def _convert_name(self, name):
        return '{table}_{name}'.format(table=self.__table, name=name)

    def _convert_id(self, name):
        return '{name}_id'.format(name=name)

    def _instance_load(self, instance, source):
        instance.__fields = dict(source)
        instance.__modified = False
        instance.__loaded = True

    def _instance_multiload(self, cls, row_list):
        temp = cls()
        instance_list = []

        for row in row_list:
            instance = cls(row[temp._convert_name('id')])
            instance._instance_load(instance, row)
            instance_list.append(instance)

        return instance_list

    def _get_class(self, name):
        return getattr(models, name)

    @classmethod
    def all(cls):
        temp = cls()
        query = cls.__list_query.format(
            table=temp.__class__.__name__.lower()
        )
        row_list = temp.__execute_query(query)

        return temp._instance_multiload(cls, row_list)

    def delete(self):
        if self.__id is None:
            raise AttributeError

        query = self.__class__.__delete_query.format(
            table=self.__table
        )
        self.__execute_query(query, [self.id])

        self.__cursor.close()

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, value):
        self.__id = value

    @property
    def created(self):
        return self.__fields[_convert_name('created')]

    @property
    def updated(self):
        return self.__fields[_convert_name('updated')]

    def save(self):
        if self.__modified:
            if self.id is None:
                self.__insert()
            else:
                self.__update()

            self.__modified = False

import models
